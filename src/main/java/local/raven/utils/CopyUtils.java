package local.raven.utils;

import java.util.Objects;

/**
 * Created by Raven @ 31.03.2018
 */
public class CopyUtils {

    public static<T> T deepCopy(T object) {
        Objects.requireNonNull(object, "Object is null");
        //noinspection unchecked
        return (T) new DeepCopier(object).copy();
    }

    public static<T> T[] deepCopy(T[] array) {
        Objects.requireNonNull(array, "Array is null");
        ArrayWrapper<T> wrapper = new ArrayWrapper<>();
        wrapper.arrayHolder = array;
        ArrayWrapper copy = (ArrayWrapper) new DeepCopier(wrapper).copy();
        //noinspection unchecked
        return (T[]) copy.arrayHolder;
    }

    private static class ArrayWrapper<T> {
        T[] arrayHolder;
    }
}
