package local.raven.utils;

import sun.reflect.ReflectionFactory;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Raven @ 31.03.2018
 */
class DeepCopier {
    private static ReflectionFactory reflectionFactory = ReflectionFactory.getReflectionFactory();
    private static Constructor rootConstructor;

    static {
        try {
            rootConstructor = Object.class.getDeclaredConstructor();
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException("This world is broken", e);
        }
    }

    private final Object object;

    private Cache cache;

    DeepCopier(Object object) {
        this.object = object;
    }

    Object copy() {
        cache = new Cache();
        //noinspection unchecked
        return copyInternal(object);
    }

    private Object copyInternal(Object object) {
        if (object == null || object.getClass().isEnum()) {
            return object;
        }

        Object copy = cache.getCachedCopyOf(object);
        if (copy != null) {
            return copy;
        }

        Class objectClass = object.getClass();
        if (objectClass.isArray()) {
            return copyArray(object);
        }

        Object newObject = getBlankObject(objectClass);
        cache.put(object, newObject);
        Field[] objectFields = objectClass.getDeclaredFields();
        Arrays.stream(objectFields)
                .filter(this::notStatic)
                .map(this::removeFinal)
                .forEach(field -> copyField(field, object, newObject));

        return newObject;
    }

    private Object copyArray(Object array) {
        Class componentType = array.getClass().getComponentType();
        int length = Array.getLength(array);
        Object newArray = Array.newInstance(componentType, length);
        if (componentType.isPrimitive()) {
            //noinspection SuspiciousSystemArraycopy
            System.arraycopy(array, 0, newArray, 0, length);
        } else {
            Object[] oldArray = (Object[]) array;
            @SuppressWarnings("MismatchedReadAndWriteOfArray")
            Object[] castedNewArray = (Object[]) newArray;
            for (int i = 0; i < oldArray.length; i++) {
                castedNewArray[i] = componentType.cast(copyInternal(oldArray[i]));
            }
        }
        return newArray;
    }

    private Object getBlankObject(Class objClass) {
        Constructor objectConstructor = reflectionFactory.newConstructorForSerialization(objClass, rootConstructor);
        try {
            return objectConstructor.newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException("Cannot instantiate " + objClass, e);
        }
    }

    private boolean notStatic(Field field) {
        return !Modifier.isStatic(field.getModifiers());
    }

    private Field removeFinal(Field field) {
        field.setAccessible(true);
        try {
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException("Cannot remove 'final' modifier", e);
        }
        return field;
    }

    private void copyField(Field field, Object oldObject, Object newObject) {
        try {
            Object value = field.get(oldObject);
            field.set(newObject, field.getType().isPrimitive() ? value : copyInternal(value));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private static class Cache {
        private final ArrayList<Object> knownObjects = new ArrayList<>();
        private final ArrayList<Object> copies = new ArrayList<>();

        Object getCachedCopyOf(Object object) {
            for (int i = 0, size = knownObjects.size(); i < size; i++) {
                Object knownObject = knownObjects.get(i);
                if (knownObject == object) {
                    return copies.get(i);
                }
            }
            return null;
        }

        void put(Object original, Object copy) {
            knownObjects.add(original);
            copies.add(copy);
        }
    }
}
