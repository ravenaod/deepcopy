package local.raven.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

/**
 * Created by Raven @ 31.03.2018
 */
public class CopyUtilsTest {

    @SuppressWarnings("unused")
    @Test
    public void deepCopyTest() {
        Object newObject = CopyUtils.deepCopy(new Object());
        RecursiveObject newRecObj = CopyUtils.deepCopy(new RecursiveObject());
        int newInt = CopyUtils.deepCopy(5);

        NullObject nullObject = new NullObject();
        Assert.assertEquals(nullObject, CopyUtils.deepCopy(nullObject));

        int[] intArray = new int[]{1, 2, 3, 4, 5};
        int[] newIntArray = CopyUtils.deepCopy(intArray);
        Assert.assertArrayEquals(intArray, newIntArray);

        String str = "blah";
        String newStr = CopyUtils.deepCopy(str);
        Assert.assertEquals(str, newStr);

        Object[] objArray = new Object[]{"one", "two", "three", "four", "five"};
        Object[] newObjArray = CopyUtils.deepCopy(objArray);
        Assert.assertArrayEquals(objArray, newObjArray);

        ComplexObject obj = ComplexObject.getObject();
        ComplexObject copy = CopyUtils.deepCopy(obj);
        Assert.assertEquals(obj, copy);

        String ten = "10";
        SomeClass someClass = new SomeClass(ten);
        SomeClass.InnerClass innerClass = someClass.new InnerClass();
        SomeClass.InnerClass innerCopy = CopyUtils.deepCopy(innerClass);
        Assert.assertEquals(ten, innerCopy.get());

        TestEnum test = TestEnum.ONE;
        TestEnum testCopy = CopyUtils.deepCopy(test);
        Assert.assertTrue(test == testCopy);
    }

    private static class RecursiveObject {
        @SuppressWarnings("unused")
        private final Object self = this;
    }

    private static class NullObject {
        @SuppressWarnings("unused")
        private final Object nullField = null;

        @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
        @Override
        public boolean equals(Object o) {
            //noinspection ConstantConditions
            return ((NullObject) o).nullField == null;
        }
    }

    private static class ComplexObject {
        Map<String, Object> map = new HashMap<>();
        TestEnum testEnum = TestEnum.TWO;

        static ComplexObject getObject() {
            ComplexObject object = new ComplexObject();
            Set<String> set = new HashSet<>();
            set.add("one");
            set.add("two");
            set.add("three");
            List<Object> list = new ArrayList<>();
            list.add(null);
            list.add(42);
            list.add("blah");
            list.add(set);
            object.map.put("list", list);
            object.map.put("set", set);
            object.map.put("null", null);
            object.map.put("devil", 666);
            return object;
        }

        @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
        @Override
        public boolean equals(Object o) {
            ComplexObject that = (ComplexObject) o;
            return map.equals(that.map) && testEnum == that.testEnum;
        }
    }

    private static class SomeClass {
        private final String value;

        SomeClass(String value) {
            this.value = value;
        }

        private class InnerClass {

            String get() {
                return SomeClass.this.value;
            }
        }
    }

    private enum TestEnum {
        ONE,
        TWO
    }
}
